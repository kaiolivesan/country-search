export const COUNTRY_CACHE = 'chosenCountry';
export const EDITED_CACHE = 'editedCountries';
export const ITEMS_PER_PAGE_DESK = 12;

export const ALL_COUNTRIES_QUERY = `
  query {
    Country(orderBy: name_asc) {
      flag {
        svgFile
      }
      _id
      name
      capital
      area
      population
      topLevelDomains {
        name
      }
      location {
        latitude
        longitude
      }
  }
}`;
