export const ALL_COUNTRIES_COORDINATES = `
  query {
    Country(orderBy: name_asc) {
      name
      location {
        latitude
        longitude
      }
  }
}`;
