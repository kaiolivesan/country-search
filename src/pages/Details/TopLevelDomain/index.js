import React from 'react';
import { useSelector } from 'react-redux';

import { Skeleton } from '@material-ui/lab';

import * as S from './styled';

const detailsSelector = (state) => state.details;
const TopLevelDomain = () => {
  const { country } = useSelector(detailsSelector);
  const { topLevelDomains } = country || {};

  if (!topLevelDomains?.length) return <Skeleton height={24} width={100} />;

  return (
    <S.TopLevelDomainText gutterBottom>Top-level domains:
      {` ${topLevelDomains.map(({ name }) => `${name}, `)}`}
    </S.TopLevelDomainText>
  );
};

export default TopLevelDomain;
