import styled from 'styled-components';

import { rem } from 'polished';

import { Typography } from '@material-ui/core';

export const Name = styled(Typography)`
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    font-size: ${rem(40)};
    margin-top: ${rem(8)};
  }`}
`;
