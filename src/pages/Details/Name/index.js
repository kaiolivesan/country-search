import React from 'react';
import { useSelector } from 'react-redux';

import { Skeleton } from '@material-ui/lab';

import * as S from './styled';

const detailsSelector = (state) => state.details;
const Name = () => {
  const { country } = useSelector(detailsSelector);
  const { name } = country || {};

  if (!name) return <Skeleton height={56} width={100} />;

  return (
    <S.Name component="h1" gutterBottom variant="h2">{name}</S.Name>
  );
};

export default Name;
