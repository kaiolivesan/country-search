import React from 'react';
import { useSelector } from 'react-redux';

import { Skeleton } from '@material-ui/lab';

import * as S from './styled';

const detailsSelector = (state) => state.details;
const Population = () => {
  const { country } = useSelector(detailsSelector);
  const { population } = country || {};

  if (!population) return <Skeleton height={24} width={100} />;

  return (
    <S.PopulationText gutterBottom>Population: {population}</S.PopulationText>
  );
};

export default Population;
