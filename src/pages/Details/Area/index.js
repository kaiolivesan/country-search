import React from 'react';
import { useSelector } from 'react-redux';

import { Skeleton } from '@material-ui/lab';

import * as S from './styled';

const detailsSelector = (state) => state.details;
const Area = () => {
  const { country } = useSelector(detailsSelector);
  const { area } = country || {};

  if (!area) return <Skeleton height={24} width={100} />;

  return (
    <S.AreaText gutterBottom>Area: {area} (km²)</S.AreaText>
  );
};

export default Area;
