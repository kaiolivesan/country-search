import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';

import Backdrop from '@Components/Backdrop';
import { initCountryDetails } from '@Store/ducks/details';

import Area from './Area';
import Capital from './Capital';
import CountryForm from './CountryForm';
import Flag from './Flag';
import Maps from './Maps';
import Name from './Name';
import Population from './Population';
import TopLevelDomain from './TopLevelDomain';

import * as S from './styled';

const detailsSelector = (state) => state.details;

const Details = () => {
  const { id } = useParams();
  const dispatch = useDispatch();

  const { isLoading, country } = useSelector(detailsSelector);

  useEffect(() => {
    dispatch(initCountryDetails({ name: id }));
  }, [dispatch, id]);

  return (
    <S.DetailsWrapper>
      <Backdrop open={isLoading} transitionDuration={{ enter: 125, exit: 125 }} />
      {country && (
        <>
          <S.DetailsIntro>
            <Flag />
            <S.DetailsText>
              <Name />
              <Capital />
              <Area />
              <Population />
              <TopLevelDomain />
            </S.DetailsText>
          </S.DetailsIntro>
          <CountryForm />
          <Maps />
        </>
      )}
    </S.DetailsWrapper>
  );
};

export default Details;
