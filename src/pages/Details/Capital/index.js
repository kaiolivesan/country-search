import React from 'react';
import { useSelector } from 'react-redux';

import { Skeleton } from '@material-ui/lab';

import * as S from './styled';

const detailsSelector = (state) => state.details;
const Capital = () => {
  const { country } = useSelector(detailsSelector);
  const { capital } = country || {};

  if (!capital) return <Skeleton height={41} width={200} />;

  return (
    <S.CapitalText component="h2" gutterBottom variant="h4">Capital: {capital}</S.CapitalText>
  );
};

export default Capital;
