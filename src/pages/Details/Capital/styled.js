import styled from 'styled-components';

import { rem } from 'polished';

import { Typography } from '@material-ui/core';

export const CapitalText = styled(Typography)`
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    font-size: ${rem(24)};
    width: 100%;
  }`}
`;
