import React from 'react';
import LazyLoad from 'react-lazyload';
import { useSelector } from 'react-redux';

import * as S from './styled';

const detailsSelector = (state) => state.details;

const Flag = () => {
  const { country } = useSelector(detailsSelector);
  const { flag, name } = country || {};

  if (!flag) return (<S.Placeholder animation="wave" component="div" height={400} width={600} />);

  return (
    <S.FlagWrapper evelation={2}>
      <LazyLoad
        height={400}
        once
        placeholder={<S.Placeholder animation="wave" component="div" height={400} width={600} />}
      >
        <img alt={name} src={flag?.svgFile} title={name} />
      </LazyLoad>
    </S.FlagWrapper>
  );
};

export default Flag;
