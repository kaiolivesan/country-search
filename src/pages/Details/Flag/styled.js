import styled from 'styled-components';

import { rem } from 'polished';

import {
  Paper,
} from '@material-ui/core';
import { Skeleton } from '@material-ui/lab';

export const FlagWrapper = styled(Paper)`
  width: calc(50% - ${rem(8)});
  height: auto;
  overflow: hidden;
  margin-right: ${rem(16)};
  position: relative;
  min-height: ${rem(400)};
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    width: 100%;
    min-height: initial;
    margin-right: 0;
  }`}

  img {
    width: 100%;
    height: auto;
    display: flex;
  }
`;

export const Placeholder = styled(Skeleton)`
  transform: scale(1);
`;
