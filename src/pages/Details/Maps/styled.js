import styled from 'styled-components';

import { rem } from 'polished';

import { Typography } from '@material-ui/core';

export const MapText = styled(Typography)``;
export const MapsWrapper = styled.div`
  position: relative;
  height: 500px;
  margin-bottom: ${rem(100)};
`;
