import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import {
  GoogleApiWrapper, Map, Marker,
} from 'google-maps-react';
import PropTypes from 'prop-types';

import { fetchCountriesCoordinates } from '@Store/ducks/map';

import * as S from './styled';

const detailsSelector = (state) => state.details;
const mapSelector = (state) => state.map;

const Maps = ({ google }) => {
  const dispatch = useDispatch();
  const [bounds, setBounds] = useState(new google.maps.LatLngBounds());
  const { country } = useSelector(detailsSelector);
  const { closestCountries } = useSelector(mapSelector);

  useEffect(() => {
    if (country) dispatch(fetchCountriesCoordinates(country?.name));
  }, [dispatch, country]);

  useEffect(() => {
    const currentBounds = new google.maps.LatLngBounds();

    closestCountries.map(({ location }) => (
      currentBounds.extend({
        lat: +location.latitude,
        lng: +location.longitude,
      })));

    setBounds(currentBounds);
  }, [google, closestCountries, dispatch]);

  return (
    <>
      {closestCountries && (
      <S.MapsWrapper>
        {country.name && <S.MapText component="h3" gutterBottom variant="h4">Five  Closest countries to {country.name}</S.MapText>}
        <Map
          bounds={bounds}
          google={google}
          initialCenter={{
            lat: -13.6616964,
            lng: -69.6606145,
          }}
          maxZoom={16}
        >
          {closestCountries.length && closestCountries.map((currCountry) => (
            <Marker
              key={currCountry.name}
              country={currCountry}
              position={{
                lat: +currCountry.location.latitude, lng: +currCountry.location.longitude,
              }}
            />
          ))}
        </Map>
      </S.MapsWrapper>
      )}
    </>
  );
};

Maps.propTypes = {
  google: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default GoogleApiWrapper({
  apiKey: process.env.SITE_NAME.GOOGLE_MAPS_API,
  libraries: ['places', 'geometry'],
})(React.memo(Maps));
