import styled from 'styled-components';

import { rem } from 'polished';

export const DetailsWrapper = styled.section`
  margin: ${rem(32)} 0;
`;

export const DetailsIntro = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: ${rem(32)};
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    flex-direction: column;
  }`}
`;

export const DetailsText = styled.div`
  width: calc(50% - ${rem(8)});
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    width: 100%;
  }`}
`;
