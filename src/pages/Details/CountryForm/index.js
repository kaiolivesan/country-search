import React, { useCallback, useState } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { useSelector, useDispatch } from 'react-redux';
import { useLocalStorage } from 'react-use';

import { EDITED_CACHE } from '@Constants/countries';

import Backdrop from '@Components/Backdrop';
import Form from '@Components/Form/Form';
import {
  FormControl, FormHelperText, InputLabel, Button, OutlinedInput, InputAdornment, Fade,
} from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';
import { Alert } from '@material-ui/lab';
import { setCountry } from '@Store/ducks/details';
import { handleEditedCountries } from '@Utils/helpers/handleEditedCountries';

import * as S from './styled';

const selector = (state) => state.details;

const CountryForm = () => {
  const dispatch = useDispatch();
  const [, setCountryStorage] = useLocalStorage('chosenCountry', {});
  const [, setEditedStorage] = useLocalStorage(EDITED_CACHE, []);
  const { country } = useSelector(selector);
  const [isLoading, setLoading] = useState(false);
  const [isSuccess, setSuccess] = useState(false);
  const {
    name, capital, area, population, topLevelDomains,
  } = country || {};
  const defaultTld = topLevelDomains?.map((tld) => tld.name).join(',');

  const {
    handleSubmit, control,
  } = useForm({
    defaultValues: {
      countryName: name,
      countryCapital: capital,
      countryArea: area,
      countryPopulation: population,
      countryTopLevelDomain: defaultTld,
    },
    mode: 'onChange',
    reValidateMode: 'onChange',
  });

  const onSubmit = useCallback((data) => {
    setLoading(true);
    setSuccess(false);
    const newConuntryInfo = {
      name: data.countryName,
      capital: data.countryCapital,
      area: data.countryArea,
      population: data.countryPopulation,
      topLevelDomains: data.countryTopLevelDomain?.split(',').map((tld) => ({ name: tld })),
    };
    dispatch(setCountry({ ...country, ...newConuntryInfo }));
    setEditedStorage(handleEditedCountries({ ...country, ...newConuntryInfo }));
    setCountryStorage({ ...country, ...newConuntryInfo });
    setLoading(false);
    setSuccess(true);
    setTimeout(() => {
      setSuccess(false);
    }, 3000);
  }, [country, dispatch, setCountryStorage, setEditedStorage]);

  if (!country) return null;

  const tldRegex = /^\.\w+(\.\w+)*$/g;
  const tldValidate = (tlds) => tlds?.length && tlds?.split(',')?.every((tld) => tld.trim().match(tldRegex));

  return (
    <S.FormCountryWrapper>
      <S.FormCountryText component="h3" gutterBottom variant="h4">Edit the country&apos;s info</S.FormCountryText>
      <Form autocomplete="off" onSubmit={handleSubmit(onSubmit)}>
        <S.FormFiedsWrapper>
          <Controller
            className="materialUIInput"
            control={control}
            name="countryName"
            render={({ field, fieldState }) => (
              <FormControl error={fieldState?.invalid} variant="outlined">
                <InputLabel htmlFor="countryName">Country name</InputLabel>
                <OutlinedInput aria-describedby="countryNameHelper" id="countryName" label="Country name" {...field} />
                {fieldState?.error && <FormHelperText id="countryNameHelper">{fieldState?.error?.message}</FormHelperText>}
              </FormControl>
            )}
            rules={{
              required: 'Empty field',
              minLength: {
                value: 3,
                message: 'This field minimum length is 3',
              },
            }}
            type="text"
          />
          <Controller
            className="materialUIInput"
            control={control}
            name="countryCapital"
            render={({ field, fieldState }) => (
              <FormControl error={fieldState?.invalid} variant="outlined">
                <InputLabel htmlFor="countryCapital">Capital</InputLabel>
                <OutlinedInput
                  aria-describedby="countryCapitalHelper"
                  id="countryCapital"
                  label="Capital"
                  {...field}
                />
                <FormHelperText id="countryCapitalHelper">{fieldState?.error?.message}</FormHelperText>
              </FormControl>
            )}
            rules={{
              required: 'Empty field',
              minLength: {
                value: 3,
                message: 'This field minimum length is 3',
              },
            }}
            type="text"
          />
          <Controller
            className="materialUIInput"
            control={control}
            name="countryArea"
            render={({ field, fieldState }) => (
              <FormControl error={fieldState?.invalid} variant="outlined">
                <InputLabel htmlFor="countryArea">Area</InputLabel>
                <OutlinedInput
                  aria-describedby="countryAreaHelper"
                  endAdornment={<InputAdornment position="end">km²</InputAdornment>}
                  id="countryArea"
                  inputProps={{ min: 1 }}
                  label="Area"
                  type="number"
                  {...field}
                />
                <FormHelperText id="countryAreaHelper">{fieldState?.error?.message}</FormHelperText>
              </FormControl>
            )}
            rules={{
              required: 'Empty field',
              min: {
                value: 1,
                message: 'This field minimum value is 1',
              },
            }}
          />
          <Controller
            className="materialUIInput"
            control={control}
            name="countryPopulation"
            render={({ field, fieldState }) => (
              <FormControl error={fieldState?.invalid} variant="outlined">
                <InputLabel htmlFor="countryPopulation">Population</InputLabel>
                <OutlinedInput
                  aria-describedby="countryPopulationHelper"
                  id="countryPopulation"
                  inputProps={{ min: 1 }}
                  label="Population"
                  min="1"
                  type="number"
                  {...field}
                />
                <FormHelperText id="countryPopulationHelper">{fieldState?.error?.message}</FormHelperText>
              </FormControl>
            )}
            rules={{
              required: 'Empty field',
              min: {
                value: 1,
                message: 'This field minimum value is 1',
              },
            }}
          />
          <Controller
            className="materialUIInput"
            control={control}
            name="countryTopLevelDomain"
            render={({ field, fieldState }) => (
              <FormControl error={fieldState?.invalid} variant="outlined">
                <InputLabel htmlFor="countryTopLevelDomain">Top Level Domain</InputLabel>
                <OutlinedInput
                  aria-describedby="countryTopLevelDomainHelper"
                  id="countryTopLevelDomain"
                  label="Top Level Domain"
                  {...field}
                />
                <FormHelperText id="countryTopLevelDomainHelper">
                  {fieldState?.error?.message || 'Top level domains separated by comma. Ex.: .com,.zw,.gov'}
                </FormHelperText>
              </FormControl>
            )}
            rules={{
              required: 'Empty field',
              validate: {
                hasValidTld: (v) => tldValidate(v) || 'invalid, insert top-level domains separeted by comma. Ex. : .com,.zw,.gov ',
              },
            }}
            type="text"
          />
        </S.FormFiedsWrapper>

        <Button
          color="primary"
          size="large"
          startIcon={<SaveIcon />}
          type="submit"
          variant="contained"
        >
          Save
        </Button>
        <Fade in={isSuccess}>
          <Alert severity="success">Country updated!</Alert>
        </Fade>
        <Backdrop open={isLoading} transitionDuration={{ enter: 125, exit: 125 }} />
      </Form>
    </S.FormCountryWrapper>
  );
};

export default CountryForm;
