import styled from 'styled-components';

import { rem } from 'polished';

import { Typography } from '@material-ui/core';

export const FormCountryText = styled(Typography)`
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    width: 100%;
    font-size: ${rem(24)};
    margin-bottom: ${rem(8)}; 
  }`}
`;

export const FormFiedsWrapper = styled.div``;

export const FormCountryWrapper = styled.section`
  .MuiAlert-standardSuccess {
    margin-top: ${rem(16)};
    width: ${rem(320)};
  }

  .MuiFormControl-root {
    width: calc(25% - ${rem(12)});

    &:not(:first-child) {
      margin-left: ${rem(16)};
      ${({ theme }) => `${theme.breakpoints.down('sm')} {
        margin-left: 0;
      }`}
    }

    &:nth-child(5) {
      margin: ${rem(16)} 0;
      width: auto;
    }

    ${({ theme }) => `${theme.breakpoints.down('sm')} {
      width: 100%;
      margin: 0 auto ${rem(16)}; 
    }`}
  }

  .MuiButton-contained {
    display: flex;
  }
`;
