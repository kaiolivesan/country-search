import styled from 'styled-components';

import { rem } from 'polished';

import { Typography } from '@material-ui/core';

export const Title = styled(Typography)`
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    margin-top:${rem(16)}; 
    font-size: ${rem(44)};
  }`}
`;

export const SubTitle = styled(Typography)`
  font-size: ${rem(24)};
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    font-size: ${rem(16)};
  }`}
`;

export const TitleWraper = styled.section`
  margin-bottom: ${rem(32)};
`;
