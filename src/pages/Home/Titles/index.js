import React from 'react';

import * as S from './styled';

const Titles = () => (
  <S.TitleWraper>
    <S.Title align="center" variant="h1">Country Search</S.Title>
    <S.SubTitle align="center">A simple way to search countries</S.SubTitle>
  </S.TitleWraper>
);

export default Titles;
