import React, { useEffect, useCallback } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { useDispatch, useSelector } from 'react-redux';

import CountryCard from '@Components/CountryCard';
// import Placeholer from '@Components/CountryCard/Placeholer';
import { Grid } from '@material-ui/core';
import { fetchCountries, setPage } from '@Store/ducks/countries';

const selector = (state) => state.countries;
// TODO: make request on demand
const CountriesList = () => {
  const dispatch = useDispatch();
  const {
    countries, page,
  } = useSelector(selector);

  useEffect(() => {
    dispatch(fetchCountries());
  }, [dispatch]);

  const handleNextPage = useCallback(() => {
    dispatch(setPage(page + 1));
    dispatch(fetchCountries());
  }, [dispatch, page]);
  const hasCountries = !!countries?.length;

  return (
    <>
      {hasCountries && (
      <Grid
        component={InfiniteScroll}
        container
        dataLength={countries?.length}
        hasMore
        next={handleNextPage}
        spacing={3}
      >
        {countries?.map((country) => (
          // eslint-disable-next-line no-underscore-dangle
          <Grid key={country._id} item lg={3} xs={12}>
            <CountryCard country={country} />
          </Grid>
        ))}

      </Grid>

      )}
    </>
  );
};

export default CountriesList;
