import styled from 'styled-components';

import { rem } from 'polished';

export const SearchWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: ${rem(24)};
  position: relative;
  z-index: 2;

  .MuiBackdrop-root {
    cursor: pointer;
  }

  form {
    width: 50%;
    ${({ theme }) => `${theme.breakpoints.down('sm')} {
      width: 100%;
    }`}
  }

  label {
    clip: rect(0 0 0 0);
    clip-path: inset(50%);
    height: 1px;
    overflow: hidden;
    position: absolute;
    white-space: nowrap;
    width: ${rem(1)};
  }

  input {
    font: inherit;
    width: 100%;
    height: ${rem(56)};
    margin: 0;
    display: block;
    padding: ${rem(6)} 0 ${rem(7)};
    background: transparent !important;
    letter-spacing: inherit;
    -webkit-tap-highlight-color: transparent;
    outline: none;
    border: 1px solid rgba(255, 255, 255, 0.23);
    border-radius: ${rem(4)};
    transition: all 10ms ease-in-out;
    padding-left: 14px;
    padding-right: 14px;
    color: #fff;
    font-weight: 400;

    &::placeholder {
      color: #fff;
      font-weight: 400;
      padding: 0;
      font-size: 1rem;
      line-height: 1;
      letter-spacing: 0.00938em;
    }

    &:hover {
      border-color: #fff;
      transition: all 10ms ease-in-out;
    }

    &:focus {
      transition: all 10ms ease-in-out;
      border-color: #90caf9 !important;
      border-width: ${rem(2)} !important;
      color: #fff;
    }
  }
`;
