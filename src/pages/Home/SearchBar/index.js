import React, { useRef, useEffect } from 'react';
import { DebounceInput } from 'react-debounce-input';
import { useDispatch, useSelector } from 'react-redux';

import Form from '@Components/Form/Form';
import {
  FormControl, FormHelperText, Backdrop,
} from '@material-ui/core';
import {
  setSearchTerm, setSearchReset, fetchSearch, setSearchActive, setSearchResults,
} from '@Store/ducks/countries';

import SearchResult from '../SearchResult';

import * as S from './styled';

const selector = (state) => state.countries;
// SEARCH IN API AND IN THE STORED COUNTRIES
const SearchBar = () => {
  const dispatch = useDispatch();
  const inputRef = useRef(null);
  const {
    isLoading, isError, searchTerm, isSearchActive,
  } = useSelector(selector);

  const handleChange = ({ target }) => dispatch(setSearchTerm(target.value));
  const handleFocus = () => dispatch(setSearchActive(true));
  const resetSearch = () => dispatch(setSearchReset());

  useEffect(() => {
    if (searchTerm) {
      dispatch(fetchSearch(searchTerm));
    } else {
      dispatch(setSearchResults([]));
    }
  }, [dispatch, searchTerm]);

  return (
    <S.SearchWrapper>
      <Backdrop onClick={resetSearch} open={isSearchActive} />
      <Form autom onSubmit={(event) => event.preventDefault()}>
        <FormControl fullWidth variant="outlined">
          <label htmlFor="searchInput">
            Country
          </label>
          <DebounceInput
            autoComplete="off"
            debounceTimeout={300}
            disabled={isLoading || isError}
            id="searchBar"
            inputRef={inputRef}
            name="searchBar"
            onChange={handleChange}
            onFocus={handleFocus}
            placeholder="Country"
            type="search"
            value={searchTerm}
          />

          <FormHelperText id="searchInputHelperText">Filter the countries by name here</FormHelperText>
        </FormControl>
      </Form>
      <SearchResult />
    </S.SearchWrapper>
  );
};

export default SearchBar;
