import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { useLocalStorage } from 'react-use';

import {
  List, ListItem, ListItemText, CircularProgress,
} from '@material-ui/core';
import { setCountry } from '@Store/ducks/details';

import * as S from './styled';

const selector = (state) => state.countries;

const SearchResult = () => {
  const dispatch = useDispatch();
  const [, setCountryStorage] = useLocalStorage('chosenCountry', {});

  const {
    searchResults, searchLoading, isSearchActive,
  } = useSelector(selector);

  const handleClick = (country) => () => {
    setCountryStorage(country);
    dispatch(setCountry(country));
  };
  const hasResults = !!searchResults?.length;

  return (
    <S.SearchResult>
      {isSearchActive && searchLoading && <CircularProgress size={30} />}
      {hasResults && (
      <List>
        {searchResults.map((country) => {
          const { name } = country;

          return (
            <ListItem key={name} button component={Link} to={`/${name}/details`}>
              <ListItemText
                onClick={handleClick(country)}
                primary={name}
                to={`/${name}/details`}
              />
            </ListItem>
          );
        })}
      </List>
      )}
    </S.SearchResult>
  );
};

export default SearchResult;
