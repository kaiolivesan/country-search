import styled from 'styled-components';

import { rem } from 'polished';

import {
  Paper,
} from '@material-ui/core';

export const SearchResult = styled(Paper)`
  position: absolute;
  width: 50%;
  max-height: ${rem(208)};
  overflow-y: scroll;
  top: ${rem(62)};
  z-index: 2;
  ${({ theme }) => `${theme.breakpoints.down('sm')} {
    width: 100%;
  }`}

  .MuiCircularProgress-root {
    margin: ${rem(8)} auto;
    display: block;
  }
`;
