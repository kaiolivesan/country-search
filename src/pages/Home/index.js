import React from 'react';

import CountriesList from './CountriesList';
import SearchBar from './SearchBar';
import Titles from './Titles';

import * as S from './styled';

const Home = () => (
  <S.HomeWrapper>
    <Titles />
    <S.CountriesContainer id="scrollableDiv">
      <SearchBar />
      <CountriesList />
    </S.CountriesContainer>
  </S.HomeWrapper>
);

export default Home;
