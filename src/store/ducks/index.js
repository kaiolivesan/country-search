import { combineReducers } from 'redux';

import countries from './countries';
import details from './details';
import map from './map';

export default combineReducers({
  countries,
  details,
  map,
});
