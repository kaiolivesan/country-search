import { COUNTRY_CACHE } from '@Constants/countries';

import { getCountries } from '@Services/country-graph-api';

import * as t from './types';
import { countryFromStorage, countryQuery } from './utils';

export const setLoading = (state) => ({
  type: t.LOADING,
  payload: state,
});

export const setSuccess = (state) => ({
  type: t.SUCCESS,
  payload: state,
});

export const setError = (state) => ({
  type: t.ERROR,
  payload: state,
});

export const setCountry = (state) => ({
  type: t.SET_COUNTRY,
  payload: state,
});

export const initCountryDetails = ({ name, _id }) => async (dispatch) => {
  dispatch(setLoading(true));
  try {
    const currentCountry = countryFromStorage(name, _id);

    let tempCountry;

    if (currentCountry) {
      tempCountry = currentCountry;
    } else {
      const { data } = await getCountries(countryQuery(name));

      tempCountry = data?.Country[0];
    }

    window.localStorage.setItem(COUNTRY_CACHE, JSON.stringify(tempCountry));
    dispatch(setCountry(tempCountry));
    dispatch(setSuccess(true));
  } catch (error) {
    dispatch(setError(true));
  } finally {
    dispatch(setLoading(false));
  }
};
