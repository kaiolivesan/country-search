import { COUNTRY_CACHE, EDITED_CACHE } from '@Constants/countries';

export const countryFromStorage = (name, id) => {
  const country = JSON.parse(localStorage.getItem(COUNTRY_CACHE));
  const editedList = JSON.parse(localStorage.getItem(EDITED_CACHE));
  const editedCountries = editedList ? editedList.filter(({ _id }) => _id === id) : [];

  if (editedCountries.length) {
    return editedCountries[0];
  }

  if (country && (country?.name === name)) {
    return country;
  }
  return false;
};

export const countryQuery = (name) => `
  query {
    Country(name: "${name}") {
      flag {
        svgFile
      }
    name
    capital
    area
    population
    topLevelDomains {
      name
    }
    location {
      latitude
      longitude
    }
  }
}`;
