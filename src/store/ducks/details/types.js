export const LOADING = `${process.env.SITE_NAME}/details/LOADING`;
export const SUCCESS = `${process.env.SITE_NAME}/details/SUCCESS`;
export const ERROR = `${process.env.SITE_NAME}/details/ERROR`;

export const SET_COUNTRY = `${process.env.SITE_NAME}/details/SET_COUNTRY`;
