import * as t from './types';

const INITIAL_STATE = {
  isLoading: false,
  isSuccess: false,
  isError: false,
  country: null,
};

export default function reducer(state = INITIAL_STATE, { type, payload }) {
  switch (type) {
    case t.LOADING:
      return { ...state, isLoading: payload };
    case t.SUCCESS:
      return { ...state, isSuccess: payload };
    case t.ERROR:
      return { ...state, isError: payload };
    case t.SET_COUNTRY:
      return { ...state, country: payload };
    default:
      return state;
  }
}
