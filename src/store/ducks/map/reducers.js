import * as t from './types';

const INITIAL_STATE = {
  isLoading: false,
  isSuccess: false,
  isError: false,
  countriesCoordinates: [],
  closestCountries: [],
};

export default function reducer(state = INITIAL_STATE, { type, payload }) {
  switch (type) {
    case t.LOADING:
      return { ...state, isLoading: payload };
    case t.SUCCESS:
      return { ...state, isSuccess: payload };
    case t.ERROR:
      return { ...state, isError: payload };
    case t.SET_CLOSEST_COUNTRIES:
      return { ...state, closestCountries: payload };
    case t.SET_COUNTRIES_COORDINATES:
      return { ...state, countriesCoordinates: payload };
    default:
      return state;
  }
}
