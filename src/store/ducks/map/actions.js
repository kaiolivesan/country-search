import { getCountries } from '@Services/country-graph-api';

import * as t from './types';
import { closestCountries, countriesCoordinates } from './utils';

export const setLoading = (state) => ({
  type: t.LOADING,
  payload: state,
});

export const setSuccess = (state) => ({
  type: t.SUCCESS,
  payload: state,
});

export const setError = (state) => ({
  type: t.ERROR,
  payload: state,
});

export const setClosestCountries = (state) => ({
  type: t.SET_CLOSEST_COUNTRIES,
  payload: state,
});

export const setCountriesCoordinates = (state) => ({
  type: t.SET_COUNTRIES_COORDINATES,
  payload: state,
});

export const fetchCountriesCoordinates = (name) => async (dispatch) => {
  dispatch(setLoading(true));

  try {
    const { data: { Country } } = await getCountries(closestCountries(name));
    const countriesToGet = Country[0]?.distanceToOtherCountries.map(({ countryName }) => (
      countryName
    ));

    const { data } = await getCountries(countriesCoordinates(countriesToGet));
    dispatch(setClosestCountries(Object.values(data).map((item) => item[0]).map((item) => item)));
    dispatch(setSuccess(true));
  } catch (error) {
    dispatch(setError(true));
  } finally {
    dispatch(setLoading(false));
  }
};
