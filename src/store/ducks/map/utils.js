export const allCountriesCoordinates = (name) => `
  query {
    Country(orderBy: name_asc, name_not: "${name}") {
      name      
      location {
        latitude
        longitude
      }
  }
}`;

export const closestCountries = (name, first = 5) => `
  query {
    Country(name: "${name}") {    
      distanceToOtherCountries(first: ${first}) {
        countryName
      }
    }
  }
`;

export const countriesCoordinates = (countryNames) => {
  const aliases = ['first', 'second', 'third', 'fourth', 'fifth'];

  const queries = countryNames.reduce((acc, curr, index) => {
    const query = `
      ${aliases[index]}: Country(name: "${curr}") {
        name
        location {
          latitude
          longitude
        }
      }
    `;

    const newAcc = acc + query;

    return newAcc;
  }, '');

  return `
    query {
      ${queries}
    }
  `;
};
