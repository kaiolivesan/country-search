export const LOADING = `${process.env.SITE_NAME}/map/LOADING`;
export const SUCCESS = `${process.env.SITE_NAME}/map/SUCCESS`;
export const ERROR = `${process.env.SITE_NAME}/map/ERROR`;

export const SET_COUNTRIES_COORDINATES = `${process.env.SITE_NAME}/map/SET_COUNTRIES_COORDINATES`;
export const SET_CLOSEST_COUNTRIES = `${process.env.SITE_NAME}/map/SET_CLOSEST_COUNTRIES`;
