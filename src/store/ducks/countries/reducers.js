import * as t from './types';

const INITIAL_STATE = {
  isLoading: false,
  isSuccess: false,
  isError: false,
  countries: [],
  page: 0,
  filteredCountries: [],
  searchTerm: '',
  searchResults: [],
  searchLoading: false,
  searchError: false,
  isSearchActive: false,
};

export default function reducer(state = INITIAL_STATE, { type, payload }) {
  switch (type) {
    case t.RESET:
      return INITIAL_STATE;
    case t.LOADING:
      return { ...state, isLoading: payload };
    case t.SUCCESS:
      return { ...state, isSuccess: payload };
    case t.ERROR:
      return { ...state, isError: payload };
    case t.SET_PAGE:
      return { ...state, page: payload };
    case t.SET_COUNTRIES:
      return { ...state, countries: payload };
    case t.SET_FILTERED_COUNTRIES:
      return { ...state, filteredCountries: payload };
    case t.SEARCH_TERM:
      return { ...state, searchTerm: payload };
    case t.SET_SEARCH_RESULTS:
      return { ...state, searchResults: payload };
    case t.SET_SEARCH_LOADING:
      return { ...state, searchLoading: payload };
    case t.SET_SEARCH_ACTIVE:
      return { ...state, isSearchActive: payload };
    case t.SET_SEARCH_ERROR:
      return { ...state, searchError: payload };
    case t.RESET_SEARCH: {
      const {
        searchTerm, searchResults, searchingLoading, isSearchActive,
      } = INITIAL_STATE;

      return {
        ...state,
        searchTerm,
        searchResults,
        searchingLoading,
        isSearchActive,
      };
    }
    default:
      return state;
  }
}
