import { ITEMS_PER_PAGE_DESK } from '@Constants/countries';

export const getQueryByTerm = (term) => `
query {
  Country(orderBy: name_asc, filter: {name_regexp: "(?i).*${term}.*"}) {
    flag {
      svgFile
    }
    _id
    name
    capital
    area
    population
    topLevelDomains {
      name
    }
    location {
      latitude
      longitude
    }
  }
}
`;

export const getCountriesByPageQuery = (offset = 0) => `
query {
  Country(first: ${ITEMS_PER_PAGE_DESK}, offset: ${offset * ITEMS_PER_PAGE_DESK}, orderBy: name_asc) {
    flag {
      svgFile
    }
    _id
    name
    capital
    area
    population
    topLevelDomains {
      name
    }
    location {
      latitude
      longitude
    }
  }
}
`;
