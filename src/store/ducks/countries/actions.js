import { getCountries } from '@Services/country-graph-api';

import * as t from './types';
import { getCountriesByPageQuery, getQueryByTerm } from './utils';

export const setReset = (state) => ({
  type: t.RESET,
  payload: state,
});

export const setLoading = (state) => ({
  type: t.LOADING,
  payload: state,
});

export const setSuccess = (state) => ({
  type: t.SUCCESS,
  payload: state,
});

export const setError = (state) => ({
  type: t.ERROR,
  payload: state,
});

export const setCountries = (state) => ({
  type: t.SET_COUNTRIES,
  payload: state,
});

export const setPage = (state) => ({
  type: t.SET_PAGE,
  payload: state,
});

export const setFilteredCountries = (state) => ({
  type: t.SET_FILTERED_COUNTRIES,
  payload: state,
});

export const setSearchTerm = (state) => ({
  type: t.SEARCH_TERM,
  payload: state,
});

export const setSearchResults = (state) => ({
  type: t.SET_SEARCH_RESULTS,
  payload: state,
});

export const setSearchLoading = (state) => ({
  type: t.SET_SEARCH_LOADING,
  payload: state,
});

export const setSearchError = (state) => ({
  type: t.SET_SEARCH_ERROR,
  payload: state,
});

export const setSearchActive = (state) => ({
  type: t.SET_SEARCH_ACTIVE,
  payload: state,
});

export const setSearchReset = (state) => ({
  type: t.RESET_SEARCH,
  payload: state,
});

export const fetchCountries = () => async (dispatch, getState) => {
  dispatch(setLoading(true));
  const state = getState();
  const { countries } = state;

  try {
    const { data } = await getCountries(getCountriesByPageQuery(countries.page));

    dispatch(setCountries([...countries.countries, ...data?.Country]));
    dispatch(setSuccess(true));
  } catch (error) {
    dispatch(setError(true));
  } finally {
    dispatch(setLoading(false));
  }
};

export const fetchSearch = (term) => async (dispatch) => {
  dispatch(setSearchLoading(true));

  try {
    const { data } = await getCountries(getQueryByTerm(term));
    dispatch(setSearchResults(data.Country));
    dispatch(setSuccess(true));
  } catch (error) {
    dispatch(setSearchError(true));
  } finally {
    dispatch(setSearchLoading(false));
  }
};
