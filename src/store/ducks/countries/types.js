export const RESET = `${process.env.SITE_NAME}/countries/RESET`;
export const LOADING = `${process.env.SITE_NAME}/countries/LOADING`;
export const SUCCESS = `${process.env.SITE_NAME}/countries/SUCCESS`;
export const ERROR = `${process.env.SITE_NAME}/countries/ERROR`;

export const SET_COUNTRIES = `${process.env.SITE_NAME}/countries/SET_COUNTRIES`;
export const SET_PAGE = `${process.env.SITE_NAME}/countries/SET_PAGE`;
export const SET_FILTERED_COUNTRIES = `${process.env.SITE_NAME}/countries/SET_FILTERED_COUNTRIES`;

export const SEARCH_TERM = `${process.env.SITE_NAME}/countries/SEARCH_TERM`;
export const SET_SEARCH_RESULTS = `${process.env.SITE_NAME}/countries/SET_SEARCH_RESULTS`;
export const SET_SEARCH_LOADING = `${process.env.SITE_NAME}/countries/SET_SEARCH_LOADING`;
export const SET_SEARCH_ACTIVE = `${process.env.SITE_NAME}/countries/SET_SEARCH_ACTIVE`;
export const SET_SEARCH_ERROR = `${process.env.SITE_NAME}/countries/SET_SEARCH_ERROR`;
export const RESET_SEARCH = `${process.env.SITE_NAME}/countries/RESET_SEARCH`;
