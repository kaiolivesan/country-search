import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createLogger } from 'redux-logger';
import thunk from 'redux-thunk';

import rootReducers from './ducks';

const isProduction = process.env.NODE_ENV === 'production';

const logger = createLogger({
  collapsed: true,
  predicate: (_, action) => (
    // action.type // All logs (default)
    action.type.match(/filters/gi) // Conditional log
  ),
});

const middlewares = [
  thunk,
  !isProduction && logger,
].filter(Boolean);

const allStoreEnhancers = !isProduction
  ? composeWithDevTools(applyMiddleware(...middlewares))
  : applyMiddleware(...middlewares);
const store = createStore(rootReducers, allStoreEnhancers);

export default store;
