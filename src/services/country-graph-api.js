// Documentation: https://github.com/lennertVanSever/graphcountries
// Demo: https://bit.ly/3cw9zD9

export const getCountries = async (query) => {
  const url = 'http://testefront.dev.softplan.com.br/';

  const data = await fetch(url, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ query }),
  });

  const json = await data.json();

  return json;
};
