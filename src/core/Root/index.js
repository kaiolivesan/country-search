import React from 'react';
import { Provider } from 'react-redux';

import { ThemeProvider } from 'styled-components';

import PropTypes from 'prop-types';

import { childrenProps } from '@Types/reactTypes';

import {
  CssBaseline, ThemeProvider as MuiThemeProvider, createMuiTheme, Container,
} from '@material-ui/core';
import store from '@Store';

const Root = ({ name, children }) => {
  const theme = createMuiTheme({
    palette: {
      type: 'dark',
      primary: {
        main: '#90caf9',
      },
    },
  });

  return (
    <Provider name={name} store={store}>
      <MuiThemeProvider theme={theme}>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <Container component="main" maxWidth="lg">
            {React.Children.only(children)}
          </Container>
        </ThemeProvider>
      </MuiThemeProvider>
    </Provider>
  );
};

Root.propTypes = {
  name: PropTypes.string,
  children: childrenProps.isRequired,
};

Root.defaultProps = {
  name: 'RootComponent',
};

export default Root;
