import { EDITED_CACHE } from '@Constants/countries';

export const handleEditedCountries = (country) => {
  const editedCountries = JSON.parse(window.localStorage.getItem(EDITED_CACHE));

  if (!editedCountries?.length) return [country];

  // eslint-disable-next-line no-underscore-dangle
  return [...editedCountries.filter(({ _id }) => _id !== country._id), country];
};
