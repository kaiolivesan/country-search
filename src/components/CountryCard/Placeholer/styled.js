import styled from 'styled-components';

import { rem } from 'polished';

export const PlaceholderWrapper = styled.div`
  padding: ${rem(16)};
`;
