import React from 'react';

import Skeleton from '@material-ui/lab/Skeleton';

import * as S from './styled';

const Placeholder = () => (
  <>
    <Skeleton animation="wave" height={193} variant="rect" width={290} />
    <S.PlaceholderWrapper>
      <Skeleton height={40} width="60%" />
      <Skeleton height={20} width="40%" />
      <Skeleton height={46} width="30%" />
    </S.PlaceholderWrapper>
  </>
);

export default Placeholder;
