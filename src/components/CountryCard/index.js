import React from 'react';
import LazyLoad from 'react-lazyload';
import { useDispatch } from 'react-redux';
import { Link as RouterLink } from 'react-router-dom';
import { useLocalStorage } from 'react-use';

import PropTypes from 'prop-types';

import {
  Card, CardActionArea, CardMedia, CardContent, Typography, CardActions, Button,
} from '@material-ui/core';
import { setCountry } from '@Store/ducks/details';

import Placeholder from './Placeholer';

const CountryCard = ({ country }) => {
  const dispatch = useDispatch();
  const [, setCountryStorage] = useLocalStorage('chosenCountry', {});
  const handleClick = () => {
    setCountryStorage(country);
    dispatch(setCountry(country));
  };
  const {
    capital, flag, name,
  } = country;

  return (
    <Card>
      <LazyLoad
        height={280}
        offset={-100}
        once
        placeholder={<Placeholder />}
      >
        <CardActionArea component={RouterLink} onClick={handleClick} to={`/${name}/details`}>
          <CardMedia
            alt={name}
            component="img"
            // the height is 2/3 of the width
            height="193"
            image={flag?.svgFile}
            title={name}
          />

          <CardContent>
            <Typography component="h2" gutterBottom variant="h5">
              {name}
            </Typography>
            <Typography color="textSecondary" component="p" variant="body2">
              Capital: {capital}
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions>
          <Button color="primary" component={RouterLink} onClick={handleClick} size="small" to={`/${name}/details`}>
            See Details
          </Button>
        </CardActions>
      </LazyLoad>
    </Card>
  );
};

CountryCard.propTypes = {
  country: PropTypes.isRequired,
  capital: PropTypes.string,
  flag: PropTypes.string,
  name: PropTypes.string,
};

CountryCard.defaultProps = {
  capital: 'capital not found',
  flag: 'flag not found',
  name: 'name not found',
};

CountryCard.displayName = 'CountryCard';

export default React.memo(CountryCard);
