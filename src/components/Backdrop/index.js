import React from 'react';

import {
  Backdrop as MuiBackdrop, CircularProgress,
} from '@material-ui/core';

const Backdrop = React.forwardRef(({
  ...props
}, ref) => (
  <MuiBackdrop ref={ref} {...props}>
    <CircularProgress size={120} />
  </MuiBackdrop>
));

Backdrop.displayName = 'Backdrop';

export default React.memo(Backdrop);
