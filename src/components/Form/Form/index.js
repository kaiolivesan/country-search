import React from 'react';

import PropTypes from 'prop-types';

import { childrenProps } from '@Types/reactTypes';

import * as S from './styled';

const Form = ({ children, onSubmit, ...props }) => (
  <S.FormWrapper noValidate onSubmit={onSubmit} {...props}>
    {children}
  </S.FormWrapper>
);

Form.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  children: childrenProps.isRequired,
};

Form.displayName = 'Form';

export default Form;
