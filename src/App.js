import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

import Root from '@Core/Root';
import Details from '@Pages/Details';
import Home from '@Pages/Home';
import NotFound from '@Pages/NotFound';

function App() {
  return (
    <Root>
      <BrowserRouter>
        <Routes>
          <Route element={<Home />} path="/" />
          <Route element={<Details />} path="/:id/details" />
          <Route element={<NotFound />} path="*" />
        </Routes>
      </BrowserRouter>

    </Root>
  );
}

export default App;
