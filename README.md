# Country Search

Simple country search application using [CRA](https://create-react-app.dev/)

***

[![NPM][npm-badge]][npm-url] [![Webpack][webpack-badge]][webpack-url] [![React][react-badge]][react-url] [![Redux][redux-badge]][redux-url] [![Styled Components][styled-components-badge]][styled-components-url]

## Requirements

* [Node](https://nodejs.org/en/) >= 14.15.1
* [NPM](https://docs.npmjs.com/cli/npm) >= 6.14.8
* [Yarn](https://yarnpkg.com/lang/en/) >= 1.15.2

## Command
* `yarn start`: Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.
* `yarn build`: Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

* **Linters**
    * `yarn lint:js`: Run *Javascript* lint
    * `yarn lint:css`: Run the *CSS/SCSS* linter of **Styled Components**
    * `yarn lint`: Run all linter (*JS*, *CSS*)

## Tech Stack

* [Javascript (ES6+)](http://es6-features.org/)
* [React](https://pt-br.reactjs.org/)
    * [React Hooks](https://pt-br.reactjs.org/docs/hooks-intro.html)
    * [Redux](https://redux.js.org/)
    * [Styled Components](https://www.styled-components.com/)

## Folder Strucutre

### TODO

## Gitflow

This project uses base **Gitflow**, please follow the standards.

* **features** branches must be created from **develop**
* When a **feature** is finished, you must do a **pull request** to the branch **develop** for it to be merged
* Only **admnistrators** can merge the **develop** branch into **master**

## Commits

This project use **Commit Lint**. Please follow the rules

[CommitLint types](https://github.com/conventional-changelog/commitlint/tree/master/%40commitlint/config-conventional#type-enum)

## Linters

Linters will ensure code standardization. To test the code, run: `yarn lint`. It will run all *Javascript* and *CSS* linters

### Javascript

The project uses a linter to standardize the Javascript code. Documentation follows:

* [AirBnb/React](https://github.com/airbnb/javascript)
* [React Hooks](https://reactjs.org/docs/hooks-rules.html)
* [Promise Lint](https://github.com/xjamundx/eslint-plugin-promise)
* [Import Lint](https://github.com/benmosher/eslint-plugin-import)

To run JS linter, use `yarn lint:js`

## Author

* [Kaio Santana](https://gitlab.com/kaiolivesan)

[npm-url]: https://www.npmjs.com/
[npm-badge]: https://img.shields.io/badge/Packages-NPM-%23CB3837.svg?logo=npm&link=https://www.npmjs.com
[webpack-url]: https://webpack.js.org/
[webpack-badge]: https://img.shields.io/badge/Bundler-Webpack-%238DD6F9.svg?logo=Webpack
[react-url]: https://facebook.github.io/react/
[react-img]: ./internals/img/react.png
[react-badge]: https://img.shields.io/badge/View-React-blue.svg?logo=React
[styled-components-url]: https://styled-components.com/
[styled-components-img]: ./internals/img/styled-components.png
[styled-components-badge]: https://img.shields.io/badge/%F0%9F%92%85%20Styles-Styled%20Components-%23de9b62.svg
[webpack-url]: https://webpack.github.io/
[webpack-img]: ./internals/img/webpack.png
[redux-url]: https://redux.js.org/
[redux-img]: ./internals/img/redux.png
[redux-badge]: https://img.shields.io/badge/State-Redux-744cbc.svg?logo=Redux&logoColor=ED2B88
[eslint-url]: http://eslint.org/
[eslint-img]: ./internals/img/eslint.png
[stylelint-url]: https://stylelint.io/
[stylelint-img]: ./internals/img/stylelint.png
[yarn-url]: https://yarnpkg.com/
[yarn-img]: ./internals/img/yarn.png
