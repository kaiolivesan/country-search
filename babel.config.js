/**
 * Babel configuration
 * https://babeljs.io/docs/usage/api/
 */
module.exports = {
  presets: [
    ['@babel/preset-env', {
      // 'target' option uses .browserslist file
      useBuiltIns: 'usage', // 'usage' | 'entry' | false, defaults to false
      corejs: '3.0.0',
    }],
    '@babel/preset-react',
  ],
  plugins: [
    'dev-expression',
    ['module-resolver', {
      root: ['.'],
      alias: {
        '@Assets': './src/assets',
        '@Components': './src/components',
        '@Constants': './src/constants',
        '@Containers': './src/containers',
        '@Contexts': './src/contexts',
        '@Core': './src/core',
        '@Hoc': './src/hoc',
        '@Hooks': './src/hooks',
        '@Icons': './src/icons',
        '@Layouts': './src/layouts',
        '@Pages': './src/pages',
        '@Seo': './src/seo',
        '@Services': './src/services',
        '@Store': './src/store',
        '@Styles': './src/styles',
        '@Types': './src/types',
        '@Utils': './src/utils',
      },
    }],
    ['inline-react-svg', {
      svgo: {
        plugins: [
          {
            removeAttrs: {
              attrs: '(data-name)',
            },
          },
          {
            cleanupIDs: true,
          },
        ],
      },
    }],
    ['styled-components', {
      ssr: true,
      displayName: true,
      preprocess: false,
    }],
  ],
};
