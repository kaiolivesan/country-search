const path = require('path');

/**
 * ESLint configuration
 * http://eslint.org/docs/user-guide/configuring
 */
module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  plugins: [
    'react',
    'react-redux',
    'jsx-a11y',
    'ramda',
    'promise',
    'import',
    'import-helpers',
  ],
  extends: [
    'airbnb',
    'airbnb/hooks',
    'plugin:react/recommended',
    'plugin:react-redux/recommended',
    'plugin:jsx-a11y/recommended',
    'plugin:ramda/recommended',
    'plugin:promise/recommended',
    'plugin:import/errors',
    'plugin:import/warnings',
  ],
  parser: 'babel-eslint',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  rules: {
    // Avoid LF/CRLF on Win/Linux/Mac
    'linebreak-style': 'off',
    'import/no-unresolved': 'off',
    // For single Styled Component exports
    'import/prefer-default-export': 'off',
    'import/no-extraneous-dependencies': ['error', {
      devDependencies: true,
    }],
    // Can use dangerouslySetInnerHTML and more
    'react/no-danger': 'off',
    // Allow .js files to use JSX syntax
    // https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-filename-extension.md
    'react/jsx-filename-extension': [1, {
      extensions: ['.js', 'jsx', '.ts', '.tsx'],
    }],
    // Avoid zillion string/variables lines
    'react/jsx-one-expression-per-line': 'off',
    // Allow comp. props as spreading object
    'react/jsx-props-no-spreading': 'off',
    // Organize components props
    'react/jsx-sort-props': 'warn',
    // Allow nested and siblings label to ref id
    'jsx-a11y/label-has-associated-control': [2, {
      required: { some: ['nesting', 'id'] },
    }],
    // Import orders
    'import-helpers/order-imports': ['warn', {
      groups: [
        '/^react/',
        '/^styled/',
        'module',
        '/^@Icons|@Types|@Constants/',
        '/^@/',
        ['parent', 'sibling', 'index'],
        '/styled/',
        '/styles/',
      ],
      newlinesBetween: 'always',
      alphabetize: { order: 'asc', ignoreCase: true },
    }],
  },
  settings: {
    react: {
      version: 'detect',
    },
    'import/resolver': {
      'babel-module': {},
      node: {
        paths: [path.resolve(__dirname, 'src')],
        extensions: ['.js', '.jsx', '.ts', '.tsx'],
      },
    },
  },
  globals: {
    __DEV__: true,
  },
};
